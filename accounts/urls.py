from django.urls import path
from django.contrib.auth.views import LoginView
# from accounts.views import (
#     AccountLoginView
# )

urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
]
